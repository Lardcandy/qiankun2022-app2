const packageName = require('./package').name

module.exports = {
    devServer: {
        proxy: 'http://127.0.0.1:8082',
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        // 关闭主机检查，使微应用可以被 fetch
        disableHostCheck: true,
    },
    configureWebpack: {
        output: {
            library: `${packageName}-[name]`,
            libraryTarget: 'umd',
            jsonpFunction: `webpackJsonp_${packageName}`,

        }
    }
}