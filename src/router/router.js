// 引入vue-router对象
import Router from "vue-router";
// import app2 from '../components/app2.vue'
import about from '../components/about.vue'
// import app2 from '../components/app2.vue'
import Score from '../components/entrance/score/score.vue'
import PersonalRecord from '../components/entrance/personalRecord/personalRecord.vue'


let qk_router_prefix = ''
if(window.__POWERED_BY_QIANKUN__) {
    qk_router_prefix = '/app2'
}

// 定义路由数组
const routes = [
    {
        // path: '/app2',
        // name: 'app2',
        // component: app2,
        // children: [
        //     {
                path: qk_router_prefix+"/about",
                component: about
        //     }
        // ]
    },
    {
        path: '/score',
        component: Score
    },
    {
        path: '/personalRecord',
        component: PersonalRecord
    }
]

// 创建路由
const router = new Router({
// hash模式：createWebHashHistory，
// history模式：createWebHistory
mode: 'hash',
routes,
});


// 输出对象
 export default router;